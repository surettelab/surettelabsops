Surette Lab Data Analysis
=========================

This README is written in Markdown. If you want to edit it, a good Markdown
tutorial can be found [here](http://markdowntutorial.com/). This repository uses
the version control system called 'git'. A good tutorial for using git can be
found [here](https://www.atlassian.com/git/tutorials/setting-up-a-repository/).

Introduction
------------

The purpose of this git repository is to store information about how the Surette
lab conducts our data analysis, to make our scripts available within and outside
the lab, and to record notes about tricks we've learned, pitfalls we want to
warn others about, and just general information.

Instructions on how to use the Bitbucket interface and git to clone this
repository, add files to it, create branches, etc. can be found under the
question mark icon in the top right corner of the page. To actually take those
actions on this repository, you can use the tools available in the sidebar on
the left.

To view the various directories and files in this repository click on "Source"
in the left sidebar. To view the history of changes made to the respository,
click on "Commits".

If you really don't want to install and use git on your own computer, you can
edit existing files directly on the website by clicking on them in the "Source"
view and then selecting "Edit" from the top right corner of the document.

Jake recommends that we have specific directories, with their own Markdown or
RMarkdown files (as well as any other required scripts) for each procedure we
want to document. Jake will upload her 16S Protocol file as an example. Please
feel free to create and update files as you learn new things.
